package sessions

import (
	"gitlab.com/genned/users/service/users"
)

type Login struct {
	users.UserInfo `bson:",inline"`
	// 密码
	Password string `bson:"password" json:"password"`
}

type LoginFirst struct {
	users.UserWithID
	// 1:是首次登入 0:不是首次登入
	First int `bson:"first" json:"first"`
}
