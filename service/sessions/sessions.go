package sessions

import (
	"context"
	"encoding/gob"
	"errors"
	"net/http"

	"github.com/globalsign/mgo"
	"github.com/gorilla/sessions"
	"github.com/kidstuff/mongostore"
	"gitlab.com/genned/users/service/users"
)

func init() {
	gob.Register(&users.UserWithID{})
}

// SessionsService #path:"/sessions/"#
type SessionsService struct {
	name  string
	store *mongostore.MongoStore
	users *users.UsersService
}

func NewSessionsService(name string, db *mgo.Collection, users *users.UsersService) (*SessionsService, error) {
	store := mongostore.NewMongoStore(db, 0, true, []byte("genned/users"))
	return &SessionsService{name, store, users}, nil
}

const KeyUser = `x-user`

func (s *SessionsService) VerifyNoauth(handler http.Handler) http.Handler {
	return s.verify(handler, true)
}

func (s *SessionsService) Verify(handler http.Handler) http.Handler {
	return s.verify(handler, false)
}

func (s *SessionsService) verify(handler http.Handler, noauth bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		session, err := s.store.Get(r, s.name)
		if err != nil {
			if noauth {
				handler.ServeHTTP(w, r)
				return
			}

			http.Error(w, "请先登入", 403)
			return
		}
		user, ok := session.Values[KeyUser]
		if !ok {
			if noauth {
				handler.ServeHTTP(w, r)
				return
			}

			http.Error(w, "请先登入", 403)
			return
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, KeyUser, user)
		r = r.WithContext(ctx)

		handler.ServeHTTP(w, r)
	})
}

func (s *SessionsService) LoginNoauth(login *users.UserInfo, create bool, w http.ResponseWriter, r *http.Request) (loginFirst *LoginFirst, err error) {
	user, err := s.users.GetAccount(login.Account)

	first := 0
	if err != nil {
		if !create {
			return nil, errors.New("该用户不存在")
		}

		// 不存在 则创建用户的
		userID, err := s.users.CreateNoauth(login)
		if err != nil {
			return nil, err
		}

		user, err = s.users.Get(userID)
		if err != nil {
			return nil, err
		}
		first = 1
	}

	session, err := s.store.Get(r, s.name)
	if err != nil {
		return nil, err
	}
	user.Password = ""
	session.Values[KeyUser] = user
	err = s.store.Save(r, w, session)
	if err != nil {
		return nil, err
	}
	return &LoginFirst{
		UserWithID: *user,
		First:      first,
	}, nil
}

// Login #route:"POST /login"#
func (s *SessionsService) Login(login *Login, w http.ResponseWriter, r *http.Request) (user *users.UserWithID, err error) {
	user, err = s.users.Verify(&users.User{
		UserInfo: login.UserInfo,
		Password: login.Password,
	})
	if err != nil {
		return nil, err
	}

	session, err := s.store.Get(r, s.name)
	if err != nil {
		return nil, err
	}
	user.Password = ""
	session.Values[KeyUser] = user
	err = s.store.Save(r, w, session)
	if err != nil {
		return nil, err
	}
	return user, nil
}

// Logout #route:"POST /logout"#
func (s *SessionsService) Logout(w http.ResponseWriter, r *http.Request) (err error) {
	session, err := s.store.Get(r, s.name)
	if err != nil {
		return nil
	}

	session.Values = map[interface{}]interface{}{}
	err = s.store.Save(r, w, session)
	if err != nil {
		return err
	}
	return nil
}

// Check #route:"POST /check"#
func (s *SessionsService) Check(r *http.Request) (user *users.UserWithID, err error) {
	session, err := s.store.Get(r, s.name)
	if err != nil {
		return nil, err
	}

	userI, ok := session.Values[KeyUser]
	if !ok {
		return nil, nil
	}

	user, _ = userI.(*users.UserWithID)
	return user, nil
}

func (s *SessionsService) GetSession(r *http.Request) (*sessions.Session, error) {
	return s.store.Get(r, s.name)

}

func (s *SessionsService) SaveSession(w http.ResponseWriter, r *http.Request, session *sessions.Session) error {
	return s.store.Save(r, w, session)

}
