package user

import (
	"net/http"

	"gitlab.com/genned/users/service/sessions"
	"gitlab.com/genned/users/service/users"
)

// User #middleware:"user"#
func User(r *http.Request) (user *users.UserWithID, err error) {
	ctx := r.Context()
	userI := ctx.Value(sessions.KeyUser)
	user, _ = userI.(*users.UserWithID)

	return user, nil
}
