package users

type UserInfo struct {
	// 账号
	Account string `bson:"account,omitempty" json:"account"`

	// 手机号
	PhoneNumber string `bson:"phone_number,omitempty" json:"phone_number"`
}

type User struct {
	UserInfo `bson:",inline"`
	// 密码
	Password string `bson:"password,omitempty" json:"password"`
}
