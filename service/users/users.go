package users

import (
	"errors"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/wzshiming/password"
)

type UserWithID struct {
	ID         bson.ObjectId `bson:"_id,omitempty" json:"user_id"`
	CreateTime time.Time     `bson:"create_time,omitempty" json:"create_time"`
	User       `bson:",inline"`
}

// UsersService #path:"/users/"#
type UsersService struct {
	name string
	db   *mgo.Collection
}

func NewUsersService(name string, db *mgo.Collection) (*UsersService, error) {
	err := db.EnsureIndex(mgo.Index{Key: []string{"account"}, Unique: true})
	if err != nil {
		return nil, err
	}
	return &UsersService{name, db}, nil
}

// CreateNoauth
func (s *UsersService) CreateNoauth(user *UserInfo) (userID bson.ObjectId, err error) {
	count, _ := s.db.Find(bson.D{{"account", user.Account}}).Count()
	if count != 0 {
		return "", errors.New("账号已经存在请勿重新注册")
	}
	userID = bson.NewObjectId()
	err = s.db.Insert(UserWithID{
		ID: userID,
		User: User{
			UserInfo: *user,
		},
		CreateTime: bson.Now(),
	})
	if err != nil {
		return "", err
	}
	return userID, nil
}

// Create #route:"POST /"#
func (s *UsersService) Create(user *User) (userID bson.ObjectId, err error) {
	count, _ := s.db.Find(bson.D{{"account", user.Account}}).Count()
	if count != 0 {
		return "", errors.New("账号已经存在请勿重新注册")
	}
	userID = bson.NewObjectId()
	user.Password = password.Encrypt(user.Account + user.Password)
	err = s.db.Insert(UserWithID{
		ID:         userID,
		User:       *user,
		CreateTime: bson.Now(),
	})
	if err != nil {
		return "", err
	}
	return userID, nil
}

// Verify #route:"POST /verify"#
func (s *UsersService) Verify(user *User) (userWithID *UserWithID, err error) {
	q := s.db.Find(bson.M{"account": user.Account})
	err = q.One(&userWithID)
	if err != nil || !password.Verify(userWithID.Account+user.Password, userWithID.Password) {
		return nil, errors.New("用户名或密码错误")
	}
	userWithID.Password = ""
	return userWithID, nil
}

// Update #route:"PUT /{user_id}"#
func (s *UsersService) Update(userID bson.ObjectId /*#name:"user_id"#*/, user *User) (err error) {
	user.Password = password.Encrypt(user.Account + user.Password)
	return s.db.UpdateId(userID, user)
}

// Delete #route:"DELETE /{user_id}"#
func (s *UsersService) Delete(userID bson.ObjectId /*#name:"user_id"#*/) (err error) {
	return s.db.RemoveId(userID)
}

// Get #route:"GET /{user_id}"#
func (s *UsersService) Get(userID bson.ObjectId /*#name:"user_id"#*/) (userWithID *UserWithID, err error) {
	q := s.db.FindId(userID) //.Select(bson.D{{"password", 0}})
	err = q.One(&userWithID)
	if err != nil {
		return nil, err
	}
	return userWithID, nil
}

// GetAccount 根据账号获取数据 #route:"GET /account/{account}"#
func (s *UsersService) GetAccount(account string) (user *UserWithID, err error) {
	m := bson.D{}
	m = append(m, bson.DocElem{"account", account})

	q := s.db.Find(m)
	err = q.One(&user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

// List 获取列表 #route:"GET /"#
func (s *UsersService) List(filter string, startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */, offset, limit int) (users []*UserWithID, err error) {
	m := bson.D{}
	if filter != "" {
		m = append(m, bson.DocElem{"account", bson.RegEx{filter, "i"}})
	}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m).Skip(offset).Limit(limit) //.Select(bson.D{{"password", 0}})
	err = q.All(&users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

// Count 获取数量 #route:"GET /count"#
func (s *UsersService) Count(filter string, startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */) (count int, err error) {
	m := bson.D{}
	if filter != "" {
		m = append(m, bson.DocElem{"account", bson.RegEx{filter, "i"}})
	}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m)
	return q.Count()
}
